import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  constructor(private service: UserService) {}
  ngOnInit() {
    this.service.getUsers().subscribe(users => {console.log(users)});
  }
}
