import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
 import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  userCol: AngularFirestoreCollection<User>;
  users: Observable<any[]>;

  constructor(private afs: AngularFirestore) {
    this.users = this.afs.collection('Users').valueChanges();
   }
    getUsers() {
      return this.users;
    }
 }


